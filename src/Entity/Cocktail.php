<?php

namespace App\Entity;

use App\Repository\CocktailRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Class Cocktail
 *
 * @author  Holger Schmitt
 * @package App\Entity
 * 
 */
class Cocktail
{
    private $id;

    private $name;

    private $category;

    private $alcoholic;

    private $glass;

    private $instructions;

    private $thumb;

    private $cocktailLists;

    private $externalId;

    private $Ingredient;

    public function __construct()
    {
        $this->cocktailLists = new ArrayCollection();
        $this->Ingredient = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getAlcoholic(): ?string
    {
        return $this->alcoholic;
    }

    public function setAlcoholic(?string $alcoholic): self
    {
        $this->alcoholic = $alcoholic;

        return $this;
    }

    public function getGlass(): ?string
    {
        return $this->glass;
    }

    public function setGlass(?string $glass): self
    {
        $this->glass = $glass;

        return $this;
    }

    public function getInstructions(): ?string
    {
        return $this->instructions;
    }

    public function setInstructions(?string $instructions): self
    {
        $this->instructions = $instructions;

        return $this;
    }

    public function getThumb(): ?string
    {
        return $this->thumb;
    }

    public function setThumb(?string $thumb): self
    {
        $this->thumb = $thumb;

        return $this;
    }

    /**
     * @return Collection<int, CocktailList>
     */
    public function getCocktailLists(): Collection
    {
        return $this->cocktailLists;
    }

    public function addCocktailList(CocktailList $cocktailList): self
    {
        if (!$this->cocktailLists->contains($cocktailList)) {
            $this->cocktailLists[] = $cocktailList;
            $cocktailList->addCocktail($this);
        }

        return $this;
    }

    public function removeCocktailList(CocktailList $cocktailList): self
    {
        if ($this->cocktailLists->removeElement($cocktailList)) {
            $cocktailList->removeCocktail($this);
        }

        return $this;
    }

    public function getExternalId(): ?int
    {
        return $this->externalId;
    }

    public function setExternalId(int $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * @return Collection<int, Ingredient>
     */
    public function getIngredient(): Collection
    {
        return $this->Ingredient;
    }

    public function addIngredient(Ingredient $ingredient): self
    {
        if (!$this->Ingredient->contains($ingredient)) {
            $this->Ingredient[] = $ingredient;
            $ingredient->setCocktail($this);
        }

        return $this;
    }

    public function removeIngredient(Ingredient $ingredient): self
    {
        if ($this->Ingredient->removeElement($ingredient)) {
            // set the owning side to null (unless already changed)
            if ($ingredient->getCocktail() === $this) {
                $ingredient->setCocktail(null);
            }
        }

        return $this;
    }
}
