<?php

namespace App\Entity;

use App\Repository\IngredientRepository;

/**
 * Class Ingredient
 *
 * @author  Holger Schmitt
 * @package App\Entity
 * 
 */
class Ingredient
{

    private $id;

    private $name;

    private $measure;

    private $cocktail;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMeasure(): ?string
    {
        return $this->measure;
    }

    public function setMeasure(?string $measure): self
    {
        $this->measure = $measure;

        return $this;
    }

    public function getCocktail(): ?Cocktail
    {
        return $this->cocktail;
    }

    public function setCocktail(?Cocktail $cocktail): self
    {
        $this->cocktail = $cocktail;

        return $this;
    }

}
