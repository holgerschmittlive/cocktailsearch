<?php

namespace App\Entity;

use App\Repository\CocktailListRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Class CocktailList
 *
 * @author  Holger Schmitt
 * @package App\Entity
 * 
 */
class CocktailList
{

    private $id;

    private $searchparam;

    private $cocktails;

    public function __construct()
    {
        $this->cocktails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSearchparam(): ?string
    {
        return $this->searchparam;
    }

    public function setSearchparam(string $searchparam): self
    {
        $this->searchparam = $searchparam;

        return $this;
    }

    /**
     * @return Collection<int, Cocktail>
     */
    public function getCocktails(): Collection
    {
        return $this->cocktails;
    }

    public function addCocktail(Cocktail $cocktail): self
    {
        if (!$this->cocktails->contains($cocktail)) {
            $this->cocktails[] = $cocktail;
        }

        return $this;
    }

    public function removeCocktail(Cocktail $cocktail): self
    {
        $this->cocktails->removeElement($cocktail);

        return $this;
    }
}
