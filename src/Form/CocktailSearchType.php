<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use App\Service\CocktailSearch;
use Exception;

/**
 * Class CocktailSearchType
 *
 * @author  Holger Schmitt
 * @package App\Form
 * 
 */
class CocktailSearchType extends AbstractType
{

    private $cocktailSearch;

    public function __construct(CocktailSearch $cocktailSearch)
    {
        $this->cocktailSearch = $cocktailSearch;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('name', null, [
                'required'    => false
            ])
            ->add('ingredient', ChoiceType::class, [
                'required'    => false,
                'choices'     => $this->getIngredientFilter(),
            ]) 
            ->add('category', ChoiceType::class, [
                'required'    => false,
                'choices'     => $this->getCategoryFilter()
            ])  
            ->add('submit', SubmitType::class);
        

    }
    
    private function getIngredientFilter(): Array{
        $ingredientList = $this->cocktailSearch->getIngredientList();
        $ingredientListFilter = [];
        foreach($ingredientList as $ingredient){
            $ingredientListFilter[$ingredient->getName()] = $ingredient->getName();
        }
        return $ingredientListFilter;
    }
    
    private function getCategoryFilter(): Array{
        $categoryList = $this->cocktailSearch->getCategoryList();
        $categoryListFilter = [];
        foreach($categoryList as $category){
            $categoryListFilter[$category] = $category;
        }
        return $categoryListFilter;
    }

}