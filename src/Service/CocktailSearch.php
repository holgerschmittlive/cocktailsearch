<?php

namespace App\Service;

use App\Entity\Cocktail;
use App\Entity\CocktailList;
use App\Entity\Ingredient;

/**
 * Description of CocktailSearch
 */
class CocktailSearch
{
    
    /**
     * searchCocktails search by name, ingredient and category
     * 
     * @param Array $searchparam  
     *
     * @return CocktailList
     */
    public function searchCocktails(Array $searchparam): CocktailList {
        
        $cocktailList = new CocktailList();
        if(isset($searchparam["name"]) && $searchparam["name"]!=""){
            $url = $_ENV['COCKTAILDBURL'] . "/search.php";
            $cocktailList = $this->search($searchparam["name"], $url. "?s=".$searchparam["name"]);
        }
        else if(isset($searchparam["ingredient"]) && $searchparam["ingredient"]!=""){
            $url = $_ENV['COCKTAILDBURL'] . "/filter.php";
            $cocktailList = $this->search($searchparam["ingredient"], $url. "?i=".$searchparam["ingredient"],'filter');
        }
        else if(isset($searchparam["category"]) && $searchparam["category"]!=""){
            $url = $_ENV['COCKTAILDBURL'] . "/filter.php";
            $cocktailList = $this->search($searchparam["category"], $url. "?c=".$searchparam["category"],'filter');
        }
        return $cocktailList;
    }
    
    /**
     * searchlogic for cocktail search
     * 
     * @param String $search 
     * @param String $url 
     * @param String $type 
     *
     * @return CocktailList
     */
    private function search($search, $url, $type = "search"): CocktailList {
        $cocktailList = new CocktailList();
        $cocktailList->setSearchparam($search);
        $cocktailsResult = json_decode(file_get_contents($url ));
        foreach($cocktailsResult as $cdr){
            if($cdr){
                foreach($cdr as $cr){ 
                    $cocktail = new Cocktail();
                    $cocktail->setExternalId($cr->idDrink);
                    $cocktail->setName($cr->strDrink);
                    $cocktail->setThumb($cr->strDrinkThumb);
                    if($type == "search"){        
                        $cocktail->setAlcoholic($cr->strAlcoholic);
                        $cocktail->setCategory($cr->strCategory);
                        $cocktail->setGlass($cr->strGlass);
                        ;
                        $cocktail->setInstructions($cr->strInstructions);

                        for($i = 1; $i < 15; $i++){
                            if(property_exists($cr,'strIngredient' . $i)){
                                if($cr->{'strIngredient' . $i} != ""){
                                    $ingredient = new Ingredient();
                                    $ingredient->setName($cr->{'strIngredient' . $i});
                                    $ingredient->setMeasure($cr->{'strMeasure' . $i});
                                    $cocktail->addIngredient($ingredient);
                                }
                            }
                        }
                    }

                    $cocktailList->addCocktail($cocktail);
                }
            }
        }
        return $cocktailList;
    }
    
    /**
     * get cocktail by id
     * 
     * @param Integer id 
     *
     * @return CocktailList
     */
    public function getCocktailById($id): CocktailList{
        $url = $_ENV['COCKTAILDBURL'] . "/lookup.php?i=".(int)$id;
        $cocktailList = new CocktailList();
        $cocktailList = $this->search($id, $url);
        return $cocktailList;
    }
    
    /**
     * get Ingredient List
     * 
     * @return Array
     */
    public function getIngredientList(): Array{
        $url = $_ENV['COCKTAILDBURL'] . "/list.php?i=list";
        $ingredientListResult = json_decode(file_get_contents($url));
        $ingredientList = [];
        foreach($ingredientListResult->drinks as $ir){
            $ingredient = new Ingredient();
            $ingredient->setName($ir->strIngredient1);
            $ingredientList[] = $ingredient;
        }
        return $ingredientList;
    }
    
    /**
     * get Category List
     * 
     * @return Array
     */
    public function getCategoryList(): Array{
        $url = $_ENV['COCKTAILDBURL'] . "/list.php?c=list";
        $categoryListResult = json_decode(file_get_contents($url));
        $categoryList = [];
        foreach($categoryListResult->drinks as $cr){
            $categoryList[] = $cr->strCategory;
        }
        return $categoryList;
    }
}
