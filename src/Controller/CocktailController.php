<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use App\Service\CocktailSearch;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Form\CocktailSearchType;

/**
 * Class CocktailController
 *
 * @author  Holger Schmitt
 * @package App\Controller
 * 
 */
class CocktailController extends AbstractController
{
    public $cocktailSearch;

    public function __construct(CocktailSearch $cocktailSearch)
    {
        $this->cocktailSearch = $cocktailSearch;
    }
    

    /**
     * Start page
     * @Route("/", name="homepage")
     *
     * @param Request $request current request Object
     *
     * @return Response response object with view to render
     */
    public function indexAction(Request $request): Response
    {    
        $cocktailSearchRequest = $request->get('cocktail_search');
        $form = $this->createForm(CocktailSearchType::class);
        $cocktails = [];
        if($cocktailSearchRequest){
            $cocktails = $this->cocktailSearch->searchCocktails($cocktailSearchRequest); 
        }
        return $this->render('cocktails/search.html.twig',[
            'form' => $form->createView(),
            'cocktails' => $cocktails
            ]);
    }
    
    /**
     * Cocktail Detail
     * @Route("cocktail/{id}", name="cocktail")
     *
     * @param Request $request current request Object
     *
     * @return Response response object with view to render
     */
    public function cocktailAction(Request $request, $id): Response
    {    
        $cocktails = [];
        if($id){
            $cocktails = $this->cocktailSearch->getCocktailById($id); 
        }
        return $this->render('cocktails/cocktail.html.twig',[
            'cocktails' => $cocktails
            ]);
    }
}